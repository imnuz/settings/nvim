- https://openvim.com/
- https://vim-adventures.com/
- https://medium.com/actualize-network/how-to-learn-vim-a-four-week-plan-cd8b376a9b85
- http://vimgenius.com/
- https://www.labnol.org/internet/learning-vim-for-beginners/28820/
========================
COC INSTALL
========================
CocInstall
 - coc-vetur
 - coc-pytnon
 - coc-tsserver
 - coc-json
 - coc-css
 - coc-eslint
 - coc-explorer
 - coc-html
 - coc-pairs
 - coc-prettier
 - coc-snippets
 - coc-yaml

========================

ctags for tagbar

========================

 flake8 mypy black jedi neovim

========================


https://www.youtube.com/watch?v=wlR5gYd6um0
https://ctoomey.com/mastering-the-vim-language-slides.pdf

Verbs In Vim
- d : Delete
- c : Change ( delete and enter intert mode )
- > : Indent
- v : Visually select
- y : Yank ( copy )


Nonus in Vim
- w : word ( forward by a "Ward")
- b : back ( back by a "ward")
- 2j : down 2 lines
- iw : inner word ( works from anywhere in a word )
- it : inner tag ( the contents of an HTML tag )
- i" : inner quotes
- ip : inner paragraph
- as : a sentence
- f, F : find the next character
- r, T : find the next character 
- / : Search


- 0 : beginning of line
- ^ : first char of line
- $ : end of line
- last char on line : g__

- qq : start to macro
- @q : repeat macro

- # : search a word that current cursor is on
- % : move between ()
- d( or d)
- , or ; : after find a character by 'f' 

https://github.com/easymotion/vim-easymotion


- <shift>+O
- <shift>+P
- A
-  <shift>+I, and <shift>+A
- /,  N, and <shift>+N
-  * and #

== Horizental Move
- "f*" jumps to character
- "t*" jumps to behind character
- "F*" and "T*" jump backwards through results 
- ";" to jump forward and "," to jump backwards through results
- "x" to delete a character, "s" to delete character and enter insert mode
- "cw" (or "ce") to delete word and enter insert mode
- combos: "dt)", "vf)", "vt)",  "yt)", "ct)", etc.
- "D" delete rest of line, "C" delete rest of line and enter insert mode
- "S" delete entire line and enter insert mode

== Vertical Move
- "gg" to jump to the top and "G" to jump to the bottom
- ":100" or "100G" to jump to line 100
- "12j" to jump down 12 lines, "12k" to jump back up (can also combine with "w" and "b" but used less often)
- Relative numbers "set relativenumber" in .vimrc
- "{" and "}" to hop to back and forth between empty lines
- "Ctrl + u" and "Ctrl + d" to jump half-page up and half-page down
- "%" to jump to matching pair of curly braces, brackets, parentheses; but DOESN'T work on quotes
- "*i{" to do command * on insides of curly brace  e.g. "di{", "d2i{", "ci{"
- "*ip" to do command * on paragraph e.g. "cip", "vip"
- "*a[" to do command * on inside of braces INCLUDING braces e.g. "da{",
- "diw" to delete word if you're in the middle of word

== file movements, buffers, splits
- $ vim <enter>  to open vim
- ":e <folder>" to open finder (":e ." for current dir) and use tab for auto-complete
- "Ctrl + p" to open plug-in finder (fzf recommended)
- "Ctrl + ^" to jump between last two files (remember to press shift otherwise it's "Ctrl + 6")
- Tip: Have a main file that serves as hub, <Ctrl + p> to file you want to go to (spoke), and then <Ctrl + ^> back to hub (avoid hopping around in a triangle between more than 2 files)
- "Ctrl + o" and "Ctrl + i" to jump backwards and forwards through history (can be inefficient if you have to jump back a lot) 
- "mh" to set local mark that you can jump to with 'h (can use any letter) but... 
- Tip: Have only 3-4 marks with "h" being most important and "l" being least important
- Tip: Simple find command might be more efficient than local marks
- "mH" to set a global mark that you can jump to with 'H (can use any letter)
- File tree can be helpful for ramping up on a new project but less helpful once you know where you want to go 
- ThePrimeagen uses splits not tabs, and tends not to have splits open for very long
- <Ctrl + w> then "o" to close all but current buffer
- <Ctrl + w> then "v" to split vertically, <Ctrl + w> then "s" (or "n") to split horizontally
- ":resize 10" to set split to 10 rows tall, ":vertical resize 20" to set split to 20 columns wide
- <Ctrl + w> then "=" to equally spread splits
- Can remap to quickly open explorer / file tree in narrow vsplit
- <Ctrl + w> then "r" to rotate buffers, <Ctrl + w> then "H" to switch from horizontal to vertical split
- Tip: Rarely uses these commands; prefers to quickly open and close any splits to focus on one buffer

- Some background / context
- Greatest tool: Determination
- Dealing with imposter syndrome
- Learning and getting good at vim is one the best investments
- Work might never make you happy but there is satisfaction is trying to be good at something
- ":h *" to open help manual page for *
