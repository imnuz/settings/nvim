""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" " Vim Awesome => https://vimawesome.com/
"
" - Vim Cheat Sheet
"  => https://vim.rtorr.com/lang/ko/
"
" https://github.com/nickjj/dotfiles/blob/master/.vimrc
" http://jaeheeship.github.io/console/2013/11/15/vimrc-configuration.html
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

source $HOME/.config/nvim/vim-plug/plugins.vim
source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/keys/mappings.vim
source $HOME/.config/nvim/general/paths.vim


"
"" Highlight the active window even play nice with tmux splits
"
set colorcolumn=80

" Specific length for different filetypes
autocmd FileType javascript setlocal colorcolumn=120

" Logic for entering/leaving a pane
function! OnWinEnter()
    if exists('w:initial_cc')
        let &colorcolumn = w:initial_cc
    endif
endfunction
function! OnWinLeave()
    if !exists('w:initial_cc')
        let w:initial_cc=&colorcolumn
    endif
    let &colorcolumn = 0
endfunction
augroup BgHighlight
    autocmd!
    autocmd WinEnter * call OnWinEnter()
    autocmd WinLeave * call OnWinLeave()
augroup END

nnoremap <C-p> :Files<Cr>
set diffopt+=vertical

source $HOME/.config/nvim/plug-config/ale.vim
source $HOME/.config/nvim/plug-config/tcomment.vim
source $HOME/.config/nvim/plug-config/rainbow_parentheses.vim
source $HOME/.config/nvim/plug-config/vim-smooth-scroll.vim
source $HOME/.config/nvim/plug-config/git-signify.vim
source $HOME/.config/nvim/plug-config/lightline.vim
source $HOME/.config/nvim/plug-config/vim-test.vim
source $HOME/.config/nvim/plug-config/vimspector.vim
source $HOME/.config/nvim/plug-config/better-whitespace.vim


"####################################################################################################
" Nvim documentation: provider
"   => https://neovim.io/doc/user/provider.html
"####################################################################################################

silent! call repeat#set("\<Plug>MyWonderfulMap", v:count)

let custom_config_file = expand('%:p:h') . '/.vim/vimrc'
if (filereadable(custom_config_file))
    exe "source " custom_config_file
endif


" https://github.com/sum-catnip/nvim/blob/master/init.vim
" https://www.reddit.com/r/neovim/comments/io2snh/neovim_lua_config_example/
lua require 'init'

