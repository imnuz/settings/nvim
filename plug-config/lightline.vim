set laststatus=2
let g:lightline = {
            \'colorscheme': 'one',
            \ 'active': {
            \   'left': [ [ 'mode', 'paste' ],
            \               ['lightline_hunks'],
            \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ],
            \ },
            \ 'component_function': {
            \   'gitbranch': 'FugitiveHead',
            \   'filename': 'LightLineFilename',
            \  'lightline_hunks': 'lightline#hunks#composer',
            \ },
            \}
let g:lightline.separator = {
            \   'left': '', 'right': ''
            \}
let g:lightline.subseparator = {
            \   'left': '', 'right': '' 
            \}
let g:lightline#hunks#hunk_symbols = [ 'A:', 'M:', 'R:' ]
let g:lightline#hunks#exclude_filetypes = [ 'startify', 'nerdtree', 'vista_kind', 'tagbar' ]

function! LightLineFilename()
    return expand('%')
endfunction

set showtabline=2  " Show tabline
set guioptions-=e  " Don't use GUI tabline
let g:lightline.tabline          = {'left': [['buffers']], 'right': [['close']]}
let g:lightline.component_expand = {'buffers': 'lightline#bufferline#buffers'}
let g:lightline.component_type   = {'buffers': 'tabsel'}
