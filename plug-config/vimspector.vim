" let g:vimspector_enable_mappings = 'HUMAN'
let g:vimspector_install_gadgets = [ 'debugpy' ] 

let test#neovim#term_position = "vertical"

func! GotoWindow(id)
    call win_gotoid(a:id)
    MaximizerToggle
endfunction

nnoremap <leader>da :call vimspector#Launch()<CR>
nnoremap <leader>dc :call GotoWindow(g:vimspector_session_windows.code)<CR>
nnoremap <leader>dt :call GotoWindow(g:vimspector_session_windows.tagpage)<CR>
nnoremap <leader>dv :call GotoWindow(g:vimspector_session_windows.variables)<CR>
nnoremap <leader>dw :call GotoWindow(g:vimspector_session_windows.watches)<CR>
nnoremap <leader>ds :call GotoWindow(g:vimspector_session_windows.stack_trace)<CR>
nnoremap <leader>do :call GotoWindow(g:vimspector_session_windows.output)<CR>

nnoremap <leader>dx :call vimspector#Reset()<CR>
nnoremap <leader>dl :call vimspector#StepInto()<CR>
nnoremap <leader>dj :call vimspector#StepOut()<CR>
nnoremap <leader>dk :call vimspector#StepOver()<CR>
nnoremap <leader>d_ :call vimspector#Restart()<CR>
nnoremap <leader>dn :call vimspector#Continue()<CR>
nnoremap <leader>drc :call vimspector#RunToCursor()<CR>
nnoremap <leader>dbp :call vimspector#ToggleBreakpoint()<CR>
nnoremap <leader>dcbp :call vimspector#ToggleConditionalBreakpoint()<CR>
nnoremap <leader>dxbp :call vimspector#ClearBreakpoints()<CR>
nnoremap <leader>d? :call AddToWatch()<CR>

func! AddToWatch()
    let word = expand("<cexpr>")
    call vimspector#AddWatch(word)
endfunction



function! DebugStrategy(cmd)
    let testName = split(a:cmd, ' ')[-1]
    echo 'It works! Command for running tests: ' . a:cmd
    call vimspector#LaunchWithSettings( #{ configuration: 'launch_debug', TestName: testName } )
endfunction

let g:test#custom_strategies = {'debug': function('DebugStrategy')}
nnoremap <leader>dd :TestNearest -strategy=debug<CR>
