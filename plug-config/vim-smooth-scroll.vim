noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 15, 3)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 15, 3)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 10, 5)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 10, 5)<CR>
