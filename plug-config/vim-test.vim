" https://serebrov.github.io/html/2018-11-28-python-debugging-with-ipdb-pdbpp.html
" https://ipfs-sec.stackexchange.cloudflare-ipfs.com/vi/A/question/13997.html
let test#strategy='neovim'
let test#python#runner='pytest'
let test#neovim#term_position = "botright 30"
nmap <silent> t<Leader>n :TestNearest --verbose -s<CR>
nmap <silent> t<Leader>f :TestFile --verbose -s<CR>
nmap <silent> t<Leader>s :TestSuite --verbose -s<CR>
nmap <silent> t<Leader>l :TestLast --verbose -s<CR>
nmap <silent> t<Leader>v :TestVisit --verbose -s<CR>
nmap <silent> t<Leader>nd :TestNearest --verbose --pdb -s<CR>
nmap <silent> t<Leader>fd :TestFile --verbose --pdb -s<CR>
nmap <silent> t<Leader>ld :TestLast --verbose --pdb -s<CR>
" nmap <silent> t<C-n> :TestNearest --verbose -s<CR>
" nmap <silent> t<C-f> :TestFile --verbose -s<CR>
" nmap <silent> t<C-s> :TestSuite --verbose -s<CR>
" nmap <silent> t<C-l> :TestLast --verbose -s<CR>
" nmap <silent> t<C-g> :TestVisit --verbose -s<CR>


ab ip import ipdb; ipdb.set_trace()

