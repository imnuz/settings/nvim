if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    "autocmd VimEnter * PlugInstall
    autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.local/share/nvim/plugged')

" Vim Theme
Plug 'ayu-theme/ayu-vim'
Plug 'ghifarit53/tokyonight-vim'
Plug 'morhetz/gruvbox'


Plug 'tomtom/tcomment_vim'
Plug 'kassio/neoterm'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'

Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim'
Plug 'nvim-telescope/telescope-vimspector.nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" Plug 'pwntester/octo.nvim'
" DAP
Plug 'nvim-telescope/telescope-dap.nvim'
Plug 'mfussenegger/nvim-dap'
Plug 'mfussenegger/nvim-dap-python'
Plug 'theHamsta/nvim-dap-virtual-text'
Plug 'rcarriga/nvim-dap-ui'


" rainbow_parentheses
Plug 'junegunn/rainbow_parentheses.vim'

" Vim Smooth Scroll
Plug 'terryma/vim-smooth-scroll'

" vim tmux navigator
Plug 'christoomey/vim-tmux-navigator'


Plug 'mhinz/vim-signify'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'tommcdo/vim-fugitive-blame-ext'

" lightline
Plug 'itchyny/lightline.vim'
Plug 'sinetoami/lightline-hunks'
Plug 'mengelbrecht/lightline-bufferline'

" indent line
Plug 'ntpeters/vim-better-whitespace'

Plug 'terryma/vim-multiple-cursors'
Plug 'easymotion/vim-easymotion'
Plug 'Raimondi/delimitMate'

" lsp
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/nvim-compe'
Plug 'folke/lsp-colors.nvim', {'branch': 'main'}
Plug 'glepnir/lspsaga.nvim'

" ale
Plug 'dense-analysis/ale'


" vim test
Plug 'janko/vim-test'
Plug 'puremourning/vimspector'

" Plug 'ekalinin/Dockerfile.vim'
" Plug 'chr4/nginx.vim'
" Plug 'posva/vim-vue'

" Surround
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'szw/vim-maximizer'
Plug 'blueyed/vim-diminactive'
Plug 'majutsushi/tagbar'


" Plug 'iberianpig/tig-explorer.vim'
" Plug 'liuchengxu/vim-clap'
" Plug 'sbdchd/neoformat'


call plug#end()

autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif
