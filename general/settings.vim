let mapleader=" "
nnoremap <Space> <Nop>
set iskeyword+=-

filetype plugin indent on
syntax enable
syntax on
set nocompatible
set hidden
set nowrap
set smarttab
set number
set pumheight=10
set smartindent
set autoindent
set smartindent
set cursorline
set background=dark
set relativenumber
set showtabline=2
set clipboard=unnamedplus
set smarttab
set cindent
set tabstop=4
set shiftwidth=4
set expandtab
set hidden
set updatetime=300
set shortmess+=c
set signcolumn=yes
set encoding=UTF-8
set nomodeline
set splitbelow splitright
set formatoptions-=cro
set clipboard=unnamedplus
set pumheight=10
set t_Co=256
set textwidth=80
set colorcolumn=+1
set fileformat=unix
set fileencoding=utf-8
set cb=unnamed

set termguicolors     " enable true colors support

"" let g:tokyonight_style = 'night' " available: night, storm
" let g:tokyonight_enable_italic = 1

if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

"colorscheme ayu
colorscheme gruvbox

au! BufWritePost $MYVIMRC source %      " auto source when writing to init.vm alternatively you can run :source $MYVIMRC
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o


