
local saga = require 'lspsaga'

-- add your config value here
-- default value
-- use_saga_diagnostic_sign = true
-- error_sign = '',
-- warn_sign = '',
-- hint_sign = '',
-- infor_sign = '',
-- dianostic_header_icon = '   ',
-- code_action_icon = ' ',
-- code_action_prompt = {
--   enable = true,
--   sign = true,
--   sign_priority = 20,
--   virtual_text = true,
-- },
-- finder_definition_icon = '  ',
-- finder_reference_icon = '  ',
-- max_preview_lines = 10, -- preview lines of lsp_finder and definition preview
-- finder_action_keys = {
--   open = 'o', vsplit = 's',split = 'i',quit = 'q',scroll_down = '<C-f>', scroll_up = '<C-b>' -- quit can be a table
-- },
-- code_action_keys = {
--   quit = 'q',exec = '<CR>'
-- },
-- rename_action_keys = {
--   quit = '<C-c>',exec = '<CR>'  -- quit can be a table
-- },
-- definition_preview_icon = '  '
-- "single" "double" "round" "plus"
-- border_style = "single"
-- rename_prompt_prefix = '➤',
-- if you don't use nvim-lspconfig you must pass your server name and
-- the related filetypes into this table
-- like server_filetype_map = {metals = {'sbt', 'scala'}}
-- server_filetype_map = {}

saga.init_lsp_saga {
    max_preview_lines = 30, -- preview lines of lsp_finder and definition preview
    border_style = "double"
}

local map = vim.api.nvim_set_keymap
local normal_mode_silent = {silent = true, noremap = true}
local normal_mode_echo = {silent = false, noremap = true}

-- Async Lsp Finder
map('n', 'gh', ':Lspsaga lsp_finder<CR>', normal_mode_silent)

-- Code Action
map('n', '<leader>ca', ':Lspsaga code_action<CR>', normal_mode_silent)
map('v', '<leader>ca', ':<C-U>Lspsaga range_code_action', normal_mode_silent)


-- Hover Doc
map('n', 'K', ':Lspsaga hover_doc<CR>', normal_mode_silent)
-- map('n', '<C-f>', '<cmd>lua require("lspsaga.action").smart_scroll_with_saga(1)<CR>', normal_mode_silent)
-- map('n', '<C-b>', '<cmd>lua require("lspsaga.action").smart_scroll_with_saga(-1)<CR>', normal_mode_silent)
-- nnoremap <silent> <C-f> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(1)<CR>
-- nnoremap <silent> <C-b> <cmd>lua require('lspsaga.action').smart_scroll_with_saga(-1)<CR>

-- SignatureHelp
map('n', 'gs', ':Lspsaga signature_help<CR>', normal_mode_silent)


-- Rename
map('n', 'gr', ':Lspsaga rename<CR>', normal_mode_silent)


-- Preview Definition
map('n', '<leader>gd', ':Lspsaga preview_definition<CR>', normal_mode_silent)


-- Jump Diagnostic and Show Diagnostics
map('n', '<leader>cd', ':Lspsaga show_line_diagnostics<CR>', normal_mode_silent)

-- only show diagnostic if cursor is over the area
map('n', '<leader>cc', ':Lspsaga show_cursor_diagnostics', normal_mode_silent)

-- jump diagnostic
map('n', '[e', ':Lspsaga diagnostic_jump_next<CR>', normal_mode_silent)
map('n', ']e', ':Lspsaga diagnostic_jump_prev<CR>', normal_mode_silent)


-- Float Terminal
-- map('n', 'A-d', ':Lspsaga open_floaterm<CR>', normal_mode_silent)
-- map('t', 'A-d', '<C-\><C-n>:Lspsaga close_floaterm<CR>', normal_mode_silent)

