local actions = require('telescope.actions')
require('telescope').setup {
    defaults = {
        file_sorter = require('telescope.sorters').get_fzy_sorter,
        prompt_prefix = ' 🔍 ',
        color_devicons = true,

        file_previewer   = require('telescope.previewers').vim_buffer_cat.new,
        grep_previewer   = require('telescope.previewers').vim_buffer_vimgrep.new,
        qflist_previewer = require('telescope.previewers').vim_buffer_qflist.new,

        mappings = {
            i = {
                ["<C-x>"] = false,
                ["<C-q>"] = actions.send_to_qflist,
            },
        }
    },
    extensions = {
        fzy_native = {
            override_generic_sorter = false,
            override_file_sorter = true,
        }
    }
}
require('telescope').load_extension('fzy_native')


require('nvim-treesitter.configs').setup {
  highlight = {
    enable = true
  },
}

require('telescope').load_extension('dap')
-- require('dap-python').setup('~/miniconda3/bin/python')
vim.g.dap_virtual_text = true

require("dapui").setup({
  icons = {
    expanded = "▾",
    collapsed = "▸"
  },
  mappings = {
    -- Use a table to apply multiple mappings
    expand = {"<CR>", "<2-LeftMouse>"},
    open = "o",
    remove = "d",
    edit = "e",
  },
  sidebar = {
    -- open_on_start = true,
    elements = {
      -- You can change the order of elements in the sidebar
      "scopes",
      "breakpoints",
      "stacks",
      "watches"
    },
    width = 40,
    position = "left" -- Can be "left" or "right"
  },
  tray = {
    open_on_start = true,
    elements = {
      "repl"
    },
    height = 10,
    position = "bottom" -- Can be "bottom" or "top"
  },
  floating = {
    max_height = nil, -- These can be integers or a float between 0 and 1.
    max_width = nil   -- Floats will be treated as percentage of your screen.
  }
})

local map = vim.api.nvim_set_keymap
local normal_mode_silent = {silent = true, noremap = true}
local normal_mode_echo = {silent = false, noremap = true}

map('n', '<C-p>', '<CMD>lua require("telescope.builtin").find_files()<CR>', normal_mode_silent)
map('n', '<C-g>', '<CMD>lua require("telescope.builtin").live_grep({ prompt_prefix=🔍 })<CR>', normal_mode_silent)
map('n', '<leader>fb', '<CMD>lua require("telescope.builtin").buffers()<CR>', normal_mode_silent)
map('n', '<leader>fh', '<CMD>lua require("telescope.builtin").help_tags()<CR>', normal_mode_silent)
map('n', '<leader>vrc', '<CMD>lua require("plugins.telescope").search_dotfiles()<CR>', normal_mode_silent)
map('n', '<leader>gb', '<CMD>lua require("plugins.telescope").git_branches()<CR>', normal_mode_silent)
-- map('n', '<leader>gb', '<CMD>lua require("telescope.builtin").git_branches()<CR>', normal_mode_silent)
map('n', '<leader>vc', '<CMD>lua require("telescope").extensions.vimspector.configurations()<CR>', normal_mode_silent)

local M = {}
M.search_dotfiles = function()
    require("telescope.builtin").find_files({
        prompt_title = "< VimRC >",
        cwd = "~/.config/nvim/",
    })
end

M.git_branches = function()
    require("telescope.builtin").git_branches({
        attach_mappings = function(prompt_bufnr, map)
            map('i', '<c-d>', actions.git_delete_branch)
            map('n', '<c-d>', actions.git_delete_branch)
            return true
        end
    })
end

return M
