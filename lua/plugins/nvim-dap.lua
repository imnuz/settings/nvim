
require('telescope').load_extension('dap')

local map = vim.api.nvim_set_keymap
local normal_mode_silent = {silent = true, noremap = true}
local normal_mode_echo = {silent = false, noremap = true}

map('n', '<leader>dct', '<cmd>lua require"dap".continue()<CR>', normal_mode_silent)
map('n', '<leader>dsv', '<cmd>lua require"dap".step_over()<CR>', normal_mode_silent)
map('n', '<leader>dsi', '<cmd>lua require"dap".step_into()<CR>', normal_mode_silent)
map('n', '<leader>dso', '<cmd>lua require"dap".step_out()<CR>', normal_mode_silent)
map('n', '<leader>dtb', '<cmd>lua require"dap".toggle_breakpoint()<CR>', normal_mode_silent)

map('n', '<leader>dsc', '<cmd>lua require"dap.ui.variables".scopes()<CR>', normal_mode_silent)
map('n', '<leader>dhh', '<cmd>lua require"dap.ui.variables".hover()<CR>', normal_mode_silent)
map('v', '<leader>dhv', '<cmd>lua require"dap.ui.variables".visual_hover()<CR>', normal_mode_silent)

map('n', '<leader>duh', '<cmd>lua require"dap.ui.widgets".hover()<CR>', normal_mode_silent)
map('n', '<leader>duf', "<cmd>lua local widgets=require'dap.ui.widgets';widgets.centered_float(widgets.scopes)<CR>", normal_mode_silent)

map('n', '<leader>dsbr', '<cmd>lua require"dap".set_breakpoint(vim.fn.input("Breakpoint condition: "))<CR>', normal_mode_silent)
map('n', '<leader>dsbm', '<cmd>lua require"dap".set_breakpoint(nil, nil, vim.fn.input("Log point message: "))<CR>', normal_mode_silent)
map('n', '<leader>dro' , '<cmd>lua require"dap".repl.open()<CR>', normal_mode_silent)
map('n', '<leader>drl' , '<cmd>lua require"dap".repl.run_last()<CR>', normal_mode_silent)


-- telescope-dap
map('n', '<leader>dcc', '<cmd>lua require"telescope".extensions.dap.commands{}<CR>', normal_mode_silent)
map('n', '<leader>dco', '<cmd>lua require"telescope".extensions.dap.configurations{}<CR>', normal_mode_silent)
map('n', '<leader>dlb', '<cmd>lua require"telescope".extensions.dap.list_breakpoints{}<CR>', normal_mode_silent)
map('n', '<leader>dv' , '<cmd>lua require"telescope".extensions.dap.variables{}<CR>', normal_mode_silent)
map('n', '<leader>df' , '<cmd>lua require"telescope".extensions.dap.frames{}<CR>', normal_mode_silent)

-- require('dap-python').setup('~/.pyenv/versions/3.8.3/bin/python')

local dap = require('dap')

local function debugJest(testName, filename)
  print("starting " .. testName .. " in " .. filename)
  dap.run({
      type = 'python',
      request = 'launch',
      name = 'pytest launch test',
      cwd = vim.fn.getcwd(),
      runtimeArgs = {'', '/usr/local/bin/jest', '--no-coverage', '-t', testName, '--', filename},
      sourceMaps = true,
      protocol = 'inspector',
      })
end


return {
  debugJest = debugJest,
}

