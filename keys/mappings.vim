" resize window
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

map <Leader>tt :sp term://zsh<CR>
set fillchars+=vert:\ 
tnoremap <Esc> <C-\><C-n>

" Better tabbing
vnoremap < <gv
vnoremap > >gv

" Better window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" TAB in general mode will move to text buffer
nnoremap <TAB> :bnext<CR>
nnoremap <S-TAB> :bprevious<CR>
nnoremap <C-c> :bdelete<CR>
map <Leader>bd :bdelete<CR>

map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K


nnoremap <Leader>m :MaximizerToggle!<CR>

let g:neoterm_default_mod = 'botright'
let g:neoterm_size = 32
let g:neoterm_autoinsert = 1
nnoremap <C-q> :Ttoggle<CR>
inoremap <C-q> <Esc> :Ttoggle<CR>
tnoremap <C-q> <C-\><C-n> :Ttoggle<CR>


nmap <leader>gj :diffget //3<CR>
nmap <leader>gf :diffget //2<CR>
nmap <leader>gs :G<CR>
